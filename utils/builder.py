import logging, os, json as simplejson, urllib
from sqlalchemy import create_engine
from mapr.ojai.storage.ConnectionFactory import ConnectionFactory
from atlassian import Jira


class logBuilder():
    def __init__(self)->None:
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.fname = os.path.join(self.dir_path, '../log/history.log')
        self.lgi, self.lge, self.lgd = 'null', 'null', 'null'
        self.fhandler = logging.FileHandler(self.fname)
        self.fhandler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

    def log_info(self, message:str)->None:
        self.lgi = logging.getLogger(__name__)
        self.lgi.setLevel(logging.INFO)
        self.lgi.addHandler(self.fhandler)
        self.lgi.info(message)

    def log_error(self, message:str)->None:
        self.lge = logging.getLogger(__name__)
        self.lge.setLevel(logging.ERROR)
        self.lge.addHandler(self.fhandler)
        self.lge.error(message)

    def log_debug(self, message:str)->None:
        self.lgd = logging.getLogger(__name__)
        self.lgd.setLevel(logging.DEBUG)
        self.lgd.addHandler(self.fhandler)
        self.lgd.debug(message)


class ListTablesBuilder():
    def __init__(self)->None:
        with open('config.json') as f:
                self._tables = simplejson.load(f)['maprdb_path']
        self._list_tables = [v[0] for i, v in enumerate(self._tables.items())]

    def get_tables_list(self):
        return  self._tables

class OjaiBuilder():
    def __init__(self)->None:
        with open('config.json') as f:
            self._conf = simplejson.load(f)['connection']['ojai']
        self._host = self._conf['host']
        self._port = self._conf['port']
        self._username = self._conf['username']
        self._password = self._conf['password']
        self._connString = f"{self._host}:{self._port}?auth=basic&user={self._username}&password={self._password}&" \
          "ssl=false&" 
        self._conn = ConnectionFactory.get_connection(self._connString)

    def get_engine(self):
        return self._conn

    def stopper(self):
        self._conn.close()
    
    def write_df(self, df, tbl_loc = ''):
        store = self._conn.get_store(tbl_loc)
        clean_dict  = simplejson.loads(simplejson.dumps(df.to_dict('records'), ignore_nan=True))
        for doc_dict in clean_dict:
            new_document = self._conn.new_document(dictionary=doc_dict)
            store.insert_or_replace(new_document)


class JiraBuilder():
    def __init__(self)->None:
        with open('config.json') as f:
            self._conf = simplejson.load(f)['connection']['jira']
        self._url = self._conf['url']
        self._username = self._conf['username']
        self._password = self._conf['password']

    def get_jira_instance(self):
        jira_instance = Jira(
            url = self._url,
            username = self._username,
            password = self._password,
            )
        return jira_instance

class MsSqlServerBuilder():
    def __init__(self)->None:
        with open('config.json') as f:
                self._conf = simplejson.load(f)['connection']['mssqlserver']
        self._server = self._conf['server'],
        self._port = self._conf['port'],
        self._database = self._conf['database'],
        self._username = self._conf['uid'],
        self._password = self._conf['password']
        self._driver = self._conf['driver']
        # self._quoted_con =  urllib.parse.quote_plus("""DRIVER={ODBC Driver 18 for SQL Server};"""+f"""SERVER={self._server};PORT:{self._port};DATABASE={self._database};UID={self._username};PWD={self._password}""")
        # self._quoted_con = """DRIVER={ODBC Driver 18 for SQL Server};"""+f"""SERVER={self._server};PORT:{self._port};DATABASE={self._database};UID={self._username};PWD={self._password}"""

        # self._engine = create_engine('mssql+pyodbc:///?odbc_connect={}'.format(self._quoted_con))
        # self._conn = self._engine.connect()
    def get_conn_prop(self):
        return self._conf
    def get_db_conn(self):
        return self._conn

    def db_close(self):
        self._conn.close()
        self._engine.dispose()

