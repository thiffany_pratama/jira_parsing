import json,sys

def check_mapr_path(log_engine, conn = None, table_name=''):
    with open('config.json') as f:
        list_path = json.load(f)['maprdb_path']
    db_path = list_path[table_name]

    if conn.is_store_exists(db_path):
        log_engine.log_info(f"TABLE CHECKING | {table_name} | Table exists - {sys.exc_info()[0]} - {sys.exc_info()[1]}")

    else:
        conn.create_store(db_path)
    conn.close()
