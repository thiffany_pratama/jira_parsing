import pandas as pd, sys
from utils.builder import *
from utils.utils import *
from utils.jql import *
import sqlalchemy



class DataProcessor():
    def __init__(self, 
                 log_engine,
                 time_range,
                 jira_conn,
                 mssqlserver,
                 ):
        self.log_engine = log_engine
        self.tr = time_range
        self.jira_conn = jira_conn
        self.mssqlserver = mssqlserver
        
        try:
            quoted =f"""DRIVER={self.mssqlserver['driver']}""" + f""";SERVER={self.mssqlserver['server']};PORT:{self.mssqlserver['port']};DATABASE={self.mssqlserver['database']};UID={self.mssqlserver['uid']};PWD={self.mssqlserver['password']}"""
            self.engine = create_engine('mssql+pyodbc:///?odbc_connect={}'.format(quoted))
            self.conn = self.engine.connect()
            self.conn.timeout = 1
        except:
            log_engine.log_info(f"DB CONNECTION PROBLEM |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            raise
        
        self.df1 = [] # JiraIssue
        self.project_key = []
        self.subproject_key = []
        self.df2 = [] #JiraProject
        self.df3 = [] #JiraIssueTransition
        self.df4 = []
        self.df5 = [] #JiraCustomField
        self.df6 = [] #JiraFieldLabels
        
        
        self.result1 = []
    
    def df1_processing(self,):
        # for tr in self.time_range:
        self.tr = self.tr.split('||') 
        start_ = self.tr[0]
        end_ = self.tr[1]
        self.log_engine.log_info(f"PROCESSING {self.tr}")
        self.result1 = self.jira_conn.get_jira_instance().jql(f"updatedDate>='{start_}' AND updatedDate<='{end_}'"
                                            , limit=10000)
        if len(self.result1['issues'])==0:
            return
        try:
            self.df1 = pd.json_normalize(self.result1["issues"], sep = "_")
            self.df1 = self.df1.rename({'id' : '_id'}, axis=1)
            self.df1['fields_statuscategorychangedate'] = self.df1['fields_statuscategorychangedate'].values.astype('datetime64[us]')
            self.df1['fields_statuscategorychangedate'] = self.df1['fields_statuscategorychangedate'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)
            self.df1['fields_resolutiondate'] =self.df1['fields_resolutiondate'].values.astype('datetime64[us]')
            self.df1['fields_resolutiondate'] = self.df1['fields_resolutiondate'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)

            self.df1['fields_created'] = self.df1['fields_created'].values.astype('datetime64[us]')
            self.df1['fields_created'] = self.df1['fields_created'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)

            try:
                self.df1['fields_customfield_10100'] = self.df1['fields_customfield_10100'].values.astype('datetime64[us]')
                self.df1['fields_customfield_10100'] = self.df1['fields_customfield_10100'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)
            except:
                pass
            
            self.df1['fields_updated'] = self.df1['fields_updated'].values.astype('datetime64[us]')
            self.df1['fields_updated'] = self.df1['fields_updated'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)

            self.df1['_id'] = self.df1['_id'].values.astype('int64')
            self.project_key = list(set(self.df1['fields_project_key']))
            self.subproject_key = list(set(self.df1['key']))

            self.log_engine.log_info(f"JiraIssue Table Fetched ({len(self.df1)}) |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        except Exception as err:
            raise
            self.log_engine.log_info(f"Failed to get JiraIssue Table  |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
    
    def df1_write(self,):
        if len(self.df1) > 0:
            try:
                df1_final = self.df1.copy()
                df1_final['fields_fixVersions'] = ''
                df1_final['fields_labels'] =''
                df1_final['fields_customfield_10610'] = ''
                df1_final['fields_versions'] = ''
                df1_final['fields_issuelinks'] = ''
                df1_final['fields_components'] = ''
                df1_final['fields_attachment'] = ''
                df1_final['fields_subtasks'] = ''
                df1_final['fields_customfield_10115'] = ''
                df1_final['fields_comment_comments'] = ''
                df1_final['fields_worklog_worklogs'] = ''
                df1_final['fields_customfield_10120'] = ''
                df1_final = df1_final[list(set(JiraIssue_cols).intersection(set(df1_final.columns)))]


                try:
                    self.conn.execute(f"USE {self.mssqlserver['database']}")
                    self.conn.execute(JiraIssue_tbl)
                except:
                    pass
                for i in range(len(df1_final)):
                    try:
                        df1_final.iloc[i:i+1].to_sql(name="JiraIssue",if_exists='append',con = self.conn, index = False,schema='dbo')
                    except sqlalchemy.exc.IntegrityError as err:
                        pass
                    except Exception as err:
                        self.log_engine.log_info(f"JiraIssue failed {df1_final.iloc[i:i+1]['key']} |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
                        pass

                self.log_engine.log_info(f"JiraIssue is written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            except Exception as err:
                self.log_engine.log_info(f"JiraIssue not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        else:
            self.log_engine.log_info(f"JiraIssue not written |  no data")

    def df2_processing(self,):      #JiraProject
        try:
            ldf2 = []
            for pro_key in self.project_key:
                ldf2.append(pd.json_normalize(self.jira_conn.get_jira_instance().project(pro_key),sep = "_"))
            self.df2=pd.concat(ldf2, axis = 0)
            self.df2=self.df2.rename({'id' : '_id'}, axis=1)
            self.df2['_id'] = self.df2['_id'].values.astype('int64')

            self.log_engine.log_info(f"JiraProject Table Fetched ({len(self.df2)}) |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        except Exception as err:
            self.log_engine.log_info(f"Failed to get JiraProject Table |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            
            
    def df2_write(self,):
        if len(self.df2) > 0:
            try:
                self.conn.execute(f"USE {self.mssqlserver['database']}")
                try:
                    self.conn.execute(JiraProject_tbl)
                except :
                    pass
                
                self.df2['components'] = ''
                self.df2['issueTypes'] = ''
                self.df2['versions'] = ''
                self.df2 = self.df2[list(set(JiraProject_cols).intersection(set(self.df2.columns)))]

                for i in range(len(self.df2)):
                    try:
                        self.df2.iloc[i:i+1].to_sql(name="JiraProject",if_exists='append',con = self.conn, index = False,schema='dbo')
                    except sqlalchemy.exc.IntegrityError as err:
                        pass
                    except Exception as err:
                        self.log_engine.log_info(f"JiraProject not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
                self.log_engine.log_info(f"JiraProject is written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            except Exception as err:
                self.log_engine.log_info(f"JiraProject not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        else:
            self.log_engine.log_info(f"JiraProject not written |  no data")
        
    def df3_4_processing(self,):
        
        # 2.2 Single Result Issue Key
        try:
            ldf3 = []
            for subproject in self.subproject_key:
                try:
                    results3 = self.jira_conn.get_jira_instance().issue(
                            subproject, 
                            expand="changelog")
                    df4_2 = pd.json_normalize(results3["changelog"]['histories'], sep = "_")
                    for x,y in enumerate(df4_2['items']):
                        y[0]['id'] = df4_2['id'][x]

                    temp = []
                    for x in df4_2['items']:
                        for y in x:
                            temp.append(x[0])

                    df4_3 = pd.DataFrame(temp)
                    self.df4 = df4_3.merge(df4_2)
                    self.df4 = self.df4.drop('items', axis = 1)
                    self.df4['issue_key'] = subproject
                    ldf3.append(self.df4)
                except :
                    print(f"{subproject} failed to parse")
                    pass
                
            self.df3=pd.concat(ldf3, axis = 0)
            self.df3=self.df3.rename({'id' : '_id'}, axis=1)
            self.df3['_id'] = self.df3['_id'].values.astype('int64')
            self.df3['created'] = self.df3['created'].values.astype('datetime64[us]')

            self.log_engine.log_info(f"JiraIssueTransition Table Fetched ({len(self.df3)}) |  {sys.exc_info()[0]} - {sys.exc_info()[1]} ")
        except Exception as err:
            self.log_engine.log_info(f"Failed to get JiraIssueTransition Table |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            
    def df3_write(self,):
        if len(self.df3) > 0:
            try:
                self.conn.execute(f"USE {self.mssqlserver['database']}")
                self.df3 = self.df3[list(set(JiraIssueTransition_cols).intersection(set(self.df3.columns)))]
                try:
                    self.conn.execute(JiraIssueTransition_tbl)
                except:
                    pass
                for i in range(len(self.df3)):
                    try:
                        self.df3.iloc[i:i+1].to_sql(name="JiraIssueTransition",if_exists='append',con = self.conn, index = False,schema='dbo')
                    except sqlalchemy.exc.IntegrityError as err:
                        pass
                    except Exception as err:
                        self.log_engine.log_info(f"JiraIssueTransition not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
                # df3.to_sql("JiraIssueTransition", con =conn, if_exists='append', index=False)
                self.log_engine.log_info(f"JiraIssueTransition is written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            except Exception as err:
                self.log_engine.log_info(f"JiraIssueTransition not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        else:
            self.log_engine.log_info(f"JiraIssueTransition not written |  no data")
    
    def df5_processing(self,):
        try:
            self.df5 = self.df1[['key','fields_customfield_10115']].dropna().reset_index().drop('index', axis = 1)

            if len(self.df5) >0:
                ldf5 = []
                for idx,element in enumerate(self.df5['fields_customfield_10115']):
                    self.df5['fields_customfield_10115'][idx][0]['key'] = list(self.df5['key'])[idx]
                    ldf5.append(element[0])
                self.df5 = pd.json_normalize(ldf5, sep = "_")
                self.df5['_id'] = self.df5['id'].values.astype(str) + self.df5['key'] + self.df5['state']
                try:
                    self.df5['startDate'] = self.df5['startDate'].values.astype('datetime64[us]')
                    self.df5['startDate'] = self.df5['startDate'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)
                except:
                    pass
                try:
                    self.df5['endDate'] = self.df5['endDate'].values.astype('datetime64[us]')
                    self.df5['endDate'] = self.df5['endDate'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)
                except:
                    pass
                try:
                    self.df5['completeDate'] = self.df5['completeDate'].astype('datetime64[us]',errors = 'ignore')
                    self.df5['completeDate'] = self.df5['completeDate'].dt.tz_localize('UTC').dt.tz_convert('Asia/Jakarta').dt.tz_localize(None)
                except:
                    self.df5['completeDate'] = None

                self.log_engine.log_info(f"JiraCustomField Fetched ({len(self.df5)}) |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            else:
                self.log_engine.log_info(f"Failed to get JiraCustomField Table  |  no data")
                
        except Exception as err:
            self.log_engine.log_info(f"Failed to get JiraCustomField Table  |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")


    def df5_write(self,):
            # JiraCustomField
        if len(self.df5) >0 :
            try:
                self.conn.execute(f"USE {self.mssqlserver['database']}")
                self.df5 = self.df5[list(set(JiraCustomField_cols).intersection(set(self.df5.columns)))]
                try:
                    self.conn.execute(JiraCustomField_tbl)
                except:
                    pass
                for i in range(len(self.df5)):
                    try:
                        self.df5.iloc[i:i+1].to_sql(name="JiraCustomField",if_exists='append',con = self.conn, index = False,schema='dbo')
                    except sqlalchemy.exc.IntegrityError as err:
                        pass
                    except Exception as err:
                        self.log_engine.log_info(f"JiraCustomField not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")

                # df5.to_sql("JiraCustomField", con =conn, if_exists='append', index=False)
                self.log_engine.log_info(f"JiraCustomField is written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            except Exception as err:
                self.log_engine.log_info(f"JiraCustomField not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        else:
            self.log_engine.log_info(f"JiraCustomField not written |  no data")
            

    def df6_processing(self,):
        try:
            ldf6 = []
            for num,labels in enumerate(self.df1['fields_labels']):
                if len(labels) >0:
                    ldf6.append(self.df1.iloc[[num]])
            if len(ldf6) > 0:
                self.df6 = pd.concat(ldf6, axis=0).explode('fields_labels')
                self.df6['_id'] = self.df6['_id'].values.astype(str)+self.df6['fields_labels']
                self.df6['fields_fixVersions'] = ''
                self.df6['fields_customfield_10610'] = ''
                self.df6['fields_versions'] = ''
                self.df6['fields_issuelinks'] = ''
                self.df6['fields_components'] = ''
                self.df6['fields_attachment'] = ''
                self.df6['fields_subtasks'] = ''
                self.df6['fields_customfield_10115'] = ''
                self.df6['fields_comment_comments'] = ''
                self.df6['fields_worklog_worklogs'] = ''
                self.df6['fields_customfield_10120'] = ''
            else:
                pass

            self.log_engine.log_info(f"JiraFieldLabels Fetched ({len(ldf6)}) |  {sys.exc_info()[0]} - {sys.exc_info()[1]} ")
        except :
            self.log_engine.log_info(f"Failed to get JiraFieldLabels Table|  {sys.exc_info()[0]} - {sys.exc_info()[1]}")


    def df6_write(self,):
            # JiraFieldLabels
        if len(self.df6) >0 :
            try:
                self.conn.execute(f"USE {self.mssqlserver['database']}")
                self.df6 = self.df6[list(set(JiraFieldLabels_cols).intersection(set(self.df6.columns)))]
                try:
                    self.conn.execute(JiraFieldLabels_tbl)
                except:
                    pass
                for i in range(len(self.df6)):
                    try:
                        self.df6.iloc[i:i+1].to_sql(name="JiraFieldLabels",if_exists='append',con = self.conn, index = False,schema='dbo')
                    except sqlalchemy.exc.IntegrityError as err:
                        pass
                    except Exception as err:
                        self.log_engine.log_info(f"JiraFieldLabels not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")

                # df6.to_sql("JiraFieldLabels", con =conn, if_exists='append', index=False)
                self.log_engine.log_info(f"JiraFieldLabels is written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
            except Exception as err:
                self.log_engine.log_info(f"JiraFieldLabels not written |  {sys.exc_info()[0]} - {sys.exc_info()[1]}")
        else:
            self.log_engine.log_info(f"JiraFieldLabels not written |  nothing to write")

    def close_conn(self,):
        if self.conn:
            self.conn.close()
        self.engine.dispose()