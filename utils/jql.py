# start_time = '2022-01-21 15:00'
start_time = '2022-04-07 08:30'
end_time = '2022-04-08 09:00'

JiraIssue_tbl = """CREATE TABLE JiraIssue (
    expand varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [_id] bigint PRIMARY KEY,
    [self] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[key] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_statuscategorychangedate datetime NULL,
	fields_issuetype_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_subtask bit NULL,
	fields_issuetype_avatarId float NULL,
	fields_issuetype_hierarchyLevel bigint NULL,
	fields_parent_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_summary varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_id float NULL,
	fields_parent_fields_status_statusCategory_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_colorName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_subtask bit NULL,
	fields_parent_fields_issuetype_avatarId float NULL,
	fields_parent_fields_issuetype_hierarchyLevel float NULL,
	fields_timespent float NULL,
	fields_project_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_projectTypeKey varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_simplified bit NULL,
	fields_project_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_fixVersions varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimespent float NULL,
	fields_customfield_10112 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution float NULL,
	fields_customfield_10113 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10630 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10114_hasEpicLinkFieldDependency bit NULL,
	fields_customfield_10114_showField bit NULL,
	fields_customfield_10114_nonEditableReason_reason varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10114_nonEditableReason_message varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10621 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10626 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolutiondate datetime NULL,
	fields_customfield_10627 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10628 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629 float NULL,
	fields_workratio bigint NULL,
	fields_issuerestriction_shouldDisplay bit NULL,
	fields_watches_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_watches_watchCount bigint NULL,
	fields_watches_isWatching bit NULL,
	fields_lastViewed varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_created datetime NULL,
	fields_customfield_10100 datetime NULL,
	fields_priority_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10101 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10300 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_labels varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10620 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10610 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10611 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10613 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimeoriginalestimate float NULL,
	fields_timeestimate float NULL,
	fields_versions varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10615 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10616 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10617 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10618 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619 float NULL,
	fields_issuelinks varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_active bit NULL,
	fields_assignee_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_updated datetime NULL,
	fields_status_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_id bigint NULL,
	fields_status_statusCategory_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_colorName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_components varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timeoriginalestimate float NULL,
	fields_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10005 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10600 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_security varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10602 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_attachment varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10603 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimeestimate float NULL,
	fields_customfield_10604 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10605 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10606 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10607 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10608 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10609 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_summary varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_active bit NULL,
	fields_creator_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_subtasks varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_active bit NULL,
	fields_reporter_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10120 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10000 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregateprogress_progress bigint NULL,
	fields_aggregateprogress_total bigint NULL,
	fields_customfield_10001 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10200 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10641 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10400 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10632 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10115 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10116 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10634 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_environment varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10117 float NULL,
	fields_duedate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_progress_progress bigint NULL,
	fields_progress_total bigint NULL,
	fields_comment_comments varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_comment_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_comment_maxResults bigint NULL,
	fields_comment_total bigint NULL,
	fields_comment_startAt bigint NULL,
	fields_votes_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_votes_votes bigint NULL,
	fields_votes_hasVoted bit NULL,
	fields_worklog_startAt bigint NULL,
	fields_worklog_maxResults bigint NULL,
	fields_worklog_total bigint NULL,
	fields_worklog_worklogs varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500 float NULL,
	fields_customfield_10629_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority float NULL,
	fields_customfield_10619_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee float NULL,
	fields_customfield_10002 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10003 float NULL,
	fields_customfield_10004 float NULL,
	fields_customfield_10640 float NULL,
	fields_assignee_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_originalEstimate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_remainingEstimate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_originalEstimateSeconds float NULL,
	fields_timetracking_remainingEstimateSeconds float NULL,
	fields_aggregateprogress_percent float NULL,
	fields_progress_percent float NULL,
	fields_timetracking_timeSpent varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_timeSpentSeconds float NULL
        );"""
    
JiraIssue_cols = ['expand', '_id', 'self', 'key', 'fields_statuscategorychangedate'
, 'fields_issuetype_self', 'fields_issuetype_id'
, 'fields_issuetype_description', 'fields_issuetype_iconUrl'
, 'fields_issuetype_name', 'fields_issuetype_subtask'
, 'fields_issuetype_avatarId', 'fields_issuetype_hierarchyLevel'
, 'fields_parent_id', 'fields_parent_key', 'fields_parent_self'
, 'fields_parent_fields_summary', 'fields_parent_fields_status_self'
, 'fields_parent_fields_status_description'
, 'fields_parent_fields_status_iconUrl', 'fields_parent_fields_status_name'
, 'fields_parent_fields_status_id'
, 'fields_parent_fields_status_statusCategory_self'
, 'fields_parent_fields_status_statusCategory_id'
, 'fields_parent_fields_status_statusCategory_key'
, 'fields_parent_fields_status_statusCategory_colorName'
, 'fields_parent_fields_status_statusCategory_name'
, 'fields_parent_fields_priority_self'
, 'fields_parent_fields_priority_iconUrl'
, 'fields_parent_fields_priority_name', 'fields_parent_fields_priority_id'
, 'fields_parent_fields_issuetype_self', 'fields_parent_fields_issuetype_id'
, 'fields_parent_fields_issuetype_description'
, 'fields_parent_fields_issuetype_iconUrl'
, 'fields_parent_fields_issuetype_name'
, 'fields_parent_fields_issuetype_subtask'
, 'fields_parent_fields_issuetype_avatarId'
, 'fields_parent_fields_issuetype_hierarchyLevel', 'fields_timespent'
, 'fields_project_self', 'fields_project_id', 'fields_project_key'
, 'fields_project_name', 'fields_project_projectTypeKey'
, 'fields_project_simplified', 'fields_project_avatarUrls_48x48'
, 'fields_project_avatarUrls_24x24', 'fields_project_avatarUrls_16x16'
, 'fields_project_avatarUrls_32x32', 'fields_fixVersions'
, 'fields_aggregatetimespent', 'fields_customfield_10112'
, 'fields_resolution', 'fields_customfield_10113', 'fields_customfield_10630'
, 'fields_customfield_10114_hasEpicLinkFieldDependency'
, 'fields_customfield_10114_showField'
, 'fields_customfield_10114_nonEditableReason_reason'
, 'fields_customfield_10114_nonEditableReason_message'
, 'fields_customfield_10500_self', 'fields_customfield_10500_value'
, 'fields_customfield_10500_id', 'fields_customfield_10621'
, 'fields_customfield_10626', 'fields_customfield_10627'
, 'fields_resolutiondate', 'fields_customfield_10628'
, 'fields_customfield_10629', 'fields_workratio'
, 'fields_issuerestriction_shouldDisplay', 'fields_watches_self'
, 'fields_watches_watchCount', 'fields_watches_isWatching'
, 'fields_lastViewed', 'fields_created', 'fields_priority_self'
, 'fields_priority_iconUrl', 'fields_priority_name', 'fields_priority_id'
, 'fields_customfield_10100', 'fields_customfield_10101'
, 'fields_customfield_10300', 'fields_labels', 'fields_customfield_10620'
, 'fields_customfield_10610', 'fields_customfield_10611'
, 'fields_customfield_10613', 'fields_timeestimate'
, 'fields_aggregatetimeoriginalestimate', 'fields_versions'
, 'fields_customfield_10615', 'fields_customfield_10616'
, 'fields_customfield_10617', 'fields_customfield_10618'
, 'fields_customfield_10619', 'fields_issuelinks', 'fields_assignee_self'
, 'fields_assignee_accountId', 'fields_assignee_avatarUrls_48x48'
, 'fields_assignee_avatarUrls_24x24', 'fields_assignee_avatarUrls_16x16'
, 'fields_assignee_avatarUrls_32x32', 'fields_assignee_displayName'
, 'fields_assignee_active', 'fields_assignee_timeZone'
, 'fields_assignee_accountType', 'fields_updated', 'fields_status_self'
, 'fields_status_description', 'fields_status_iconUrl', 'fields_status_name'
, 'fields_status_id', 'fields_status_statusCategory_self'
, 'fields_status_statusCategory_id', 'fields_status_statusCategory_key'
, 'fields_status_statusCategory_colorName'
, 'fields_status_statusCategory_name', 'fields_components'
, 'fields_timeoriginalestimate', 'fields_description'
, 'fields_customfield_10005', 'fields_customfield_10600', 'fields_security'
, 'fields_customfield_10602', 'fields_customfield_10603'
, 'fields_aggregatetimeestimate', 'fields_attachment'
, 'fields_customfield_10604', 'fields_customfield_10605'
, 'fields_customfield_10606', 'fields_customfield_10607'
, 'fields_customfield_10608', 'fields_customfield_10609', 'fields_summary'
, 'fields_creator_self', 'fields_creator_accountId'
, 'fields_creator_avatarUrls_48x48', 'fields_creator_avatarUrls_24x24'
, 'fields_creator_avatarUrls_16x16', 'fields_creator_avatarUrls_32x32'
, 'fields_creator_displayName', 'fields_creator_active'
, 'fields_creator_timeZone', 'fields_creator_accountType', 'fields_subtasks'
, 'fields_customfield_10120', 'fields_reporter_self'
, 'fields_reporter_accountId', 'fields_reporter_avatarUrls_48x48'
, 'fields_reporter_avatarUrls_24x24', 'fields_reporter_avatarUrls_16x16'
, 'fields_reporter_avatarUrls_32x32', 'fields_reporter_displayName'
, 'fields_reporter_active', 'fields_reporter_timeZone'
, 'fields_reporter_accountType', 'fields_aggregateprogress_progress'
, 'fields_aggregateprogress_total', 'fields_customfield_10000'
, 'fields_customfield_10001', 'fields_customfield_10200'
, 'fields_customfield_10641', 'fields_customfield_10400'
, 'fields_customfield_10632', 'fields_customfield_10115'
, 'fields_customfield_10116', 'fields_environment'
, 'fields_customfield_10117', 'fields_customfield_10634', 'fields_duedate'
, 'fields_progress_progress', 'fields_progress_total'
, 'fields_comment_comments', 'fields_comment_self'
, 'fields_comment_maxResults', 'fields_comment_total'
, 'fields_comment_startAt', 'fields_votes_self', 'fields_votes_votes'
, 'fields_votes_hasVoted', 'fields_worklog_startAt'
, 'fields_worklog_maxResults', 'fields_worklog_total'
, 'fields_worklog_worklogs', 'fields_resolution_self', 'fields_resolution_id'
, 'fields_resolution_description', 'fields_resolution_name'
, 'fields_customfield_10500', 'fields_customfield_10629_self'
, 'fields_customfield_10629_value', 'fields_customfield_10629_id'
, 'fields_priority', 'fields_customfield_10619_self'
, 'fields_customfield_10619_value', 'fields_customfield_10619_id'
, 'fields_assignee', 'fields_customfield_10002', 'fields_customfield_10003'
, 'fields_customfield_10004', 'fields_customfield_10640'
, 'fields_assignee_emailAddress', 'fields_creator_emailAddress'
, 'fields_reporter_emailAddress', 'fields_timetracking_originalEstimate'
, 'fields_timetracking_remainingEstimate'
, 'fields_timetracking_originalEstimateSeconds'
, 'fields_timetracking_remainingEstimateSeconds'
, 'fields_aggregateprogress_percent', 'fields_progress_percent'
, 'fields_timetracking_timeSpent', 'fields_timetracking_timeSpentSeconds']

JiraProject_tbl = """CREATE TABLE JiraProject (
	expand varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[self] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[_id] bigint PRIMARY KEY,
	[key] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	components varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	issueTypes varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	assigneeType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	versions varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	projectTypeKey varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	simplified bit NULL,
	[style] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	isPrivate bit NULL,
	lead_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	lead_active bit NULL,
	[roles_ERP-Tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-DEV Scrum Master] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-DEV Tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR Leader] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-UAT Tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-scrum-master] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_SellerApps-Test] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-pre-uat-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-scrum-master] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-uat-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-developer] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-pre-qc-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_atlassian-addons-project-access] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-developer] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_ERP-Developer] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-pre-uat-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_ERP-Scrum-Master] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-sit-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-DEV Product Owner] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-qc-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_ERP-preuat-Tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_SellerApps-Scrum Master] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_SellerApps-BSO] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-uat-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_aman-qc-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-DEV Developer] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_SellerApps-Dev] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	roles_Developers varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_STAR-DEV Observer] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	roles_Administrators varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_acction-pre-qc-tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[roles_ERP-UAT-Tester] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);"""

JiraProject_cols = ['expand', 'self', '_id', 'key', 'description', 'components', 'issueTypes'
, 'assigneeType', 'versions', 'name', 'projectTypeKey', 'simplified', 'style'
, 'isPrivate', 'lead_self', 'lead_accountId', 'lead_avatarUrls_48x48'
, 'lead_avatarUrls_24x24', 'lead_avatarUrls_16x16', 'lead_avatarUrls_32x32'
, 'lead_displayName', 'lead_active', 'roles_ERP-Tester'
, 'roles_STAR-DEV Scrum Master', 'roles_STAR-DEV Tester', 'roles_STAR Leader'
, 'roles_STAR-UAT Tester', 'roles_aman-scrum-master', 'roles_SellerApps-Test'
, 'roles_aman-pre-uat-tester', 'roles_acction-scrum-master'
, 'roles_aman-uat-tester', 'roles_acction-developer'
, 'roles_aman-pre-qc-tester', 'roles_atlassian-addons-project-access'
, 'roles_aman-developer', 'roles_ERP-Developer'
, 'roles_acction-pre-uat-tester', 'roles_ERP-Scrum-Master'
, 'roles_acction-sit-tester', 'roles_STAR-DEV Product Owner'
, 'roles_acction-qc-tester', 'roles_ERP-preuat-Tester'
, 'roles_SellerApps-Scrum Master', 'roles_SellerApps-BSO'
, 'roles_acction-uat-tester', 'roles_aman-qc-tester'
, 'roles_STAR-DEV Developer', 'roles_SellerApps-Dev', 'roles_Developers'
, 'roles_STAR-DEV Observer', 'roles_Administrators'
, 'roles_acction-pre-qc-tester', 'roles_ERP-UAT-Tester', 'avatarUrls_48x48'
, 'avatarUrls_24x24', 'avatarUrls_16x16', 'avatarUrls_32x32']

JiraIssueTransition_tbl = """CREATE TABLE JiraIssueTransition (
	field varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fieldtype varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fieldId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[from] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fromString varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[to] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	toString varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[_id] bigint PRIMARY KEY,
	tmpFromAccountId float NULL,
	tmpToAccountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	created datetime NULL,
	author_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_active bit NULL,
    [author_emailAddress] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	author_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	issue_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);"""

JiraIssueTransition_cols =  ['field', 'fieldtype', 'fieldId', 'from', 'fromString', 'to', 'toString', '_id'
, 'tmpFromAccountId', 'tmpToAccountId', 'created', 'author_self'
, 'author_accountId', 'author_avatarUrls_48x48', 'author_avatarUrls_24x24'
, 'author_avatarUrls_16x16', 'author_avatarUrls_32x32', 'author_displayName'
, 'author_active', 'author_timeZone', 'author_accountType', 'issue_key'
, 'author_emailAddress']

JiraCustomField_tbl = """CREATE TABLE JiraCustomField (
	id bigint NULL,
	name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	state varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	boardId bigint NULL,
	goal varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	startDate datetime NULL,
	endDate datetime NULL,
	[key] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	completeDate datetime NULL,
	[_id] varchar(25) PRIMARY KEY);"""

JiraCustomField_cols =  ['id', 'name', 'state', 'boardId', 'goal', 'startDate', 'endDate', 'key'
, 'completeDate', '_id']

JiraFieldLabels_tbl = """CREATE TABLE JiraFieldLabels (
	expand varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[_id] varchar(25) PRIMARY KEY,
	[self] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[key] varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_statuscategorychangedate datetime NULL,
	fields_issuetype_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_issuetype_subtask bit NULL,
	fields_issuetype_avatarId float NULL,
	fields_issuetype_hierarchyLevel bigint NULL,
	fields_parent_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_summary varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_id float NULL,
	fields_parent_fields_status_statusCategory_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_colorName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_status_statusCategory_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_priority_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_parent_fields_issuetype_subtask bit NULL,
	fields_parent_fields_issuetype_avatarId float NULL,
	fields_parent_fields_issuetype_hierarchyLevel float NULL,
	fields_timespent float NULL,
	fields_project_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_projectTypeKey varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_simplified bit NULL,
	fields_project_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_project_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_fixVersions varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimespent float NULL,
	fields_customfield_10112 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution float NULL,
	fields_customfield_10113 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10630 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10114_hasEpicLinkFieldDependency bit NULL,
	fields_customfield_10114_showField bit NULL,
	fields_customfield_10114_nonEditableReason_reason varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10114_nonEditableReason_message varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10621 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10626 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolutiondate datetime NULL,
	fields_customfield_10627 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10628 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629 float NULL,
	fields_workratio bigint NULL,
	fields_issuerestriction_shouldDisplay bit NULL,
	fields_watches_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_watches_watchCount bigint NULL,
	fields_watches_isWatching bit NULL,
	fields_lastViewed varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_created datetime NULL,
	fields_customfield_10100 datetime NULL,
	fields_priority_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10101 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10300 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_labels varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10620 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10610 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10611 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10613 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimeoriginalestimate float NULL,
	fields_timeestimate float NULL,
	fields_versions varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10615 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10616 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10617 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10618 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619 float NULL,
	fields_issuelinks varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_active bit NULL,
	fields_assignee_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_updated datetime NULL,
	fields_status_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_iconUrl varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_id bigint NULL,
	fields_status_statusCategory_key varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_colorName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_status_statusCategory_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_components varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timeoriginalestimate float NULL,
	fields_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10005 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10600 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_security varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10602 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_attachment varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10603 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregatetimeestimate float NULL,
	fields_customfield_10604 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10605 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10606 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10607 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10608 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10609 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_summary varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_active bit NULL,
	fields_creator_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_subtasks varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_accountId varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_48x48 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_24x24 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_16x16 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_avatarUrls_32x32 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_displayName varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_active bit NULL,
	fields_reporter_timeZone varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_accountType varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10120 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10000 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_aggregateprogress_progress bigint NULL,
	fields_aggregateprogress_total bigint NULL,
	fields_customfield_10001 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10200 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10641 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10400 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10632 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10115 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10116 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10634 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_environment varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10117 float NULL,
	fields_duedate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_progress_progress bigint NULL,
	fields_progress_total bigint NULL,
	fields_comment_comments varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_comment_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_comment_maxResults bigint NULL,
	fields_comment_total bigint NULL,
	fields_comment_startAt bigint NULL,
	fields_votes_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_votes_votes bigint NULL,
	fields_votes_hasVoted bit NULL,
	fields_worklog_startAt bigint NULL,
	fields_worklog_maxResults bigint NULL,
	fields_worklog_total bigint NULL,
	fields_worklog_worklogs varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_description varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_resolution_name varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10500 float NULL,
	fields_customfield_10629_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10629_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_priority float NULL,
	fields_customfield_10619_self varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619_value varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10619_id varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_assignee float NULL,
	fields_customfield_10002 varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_customfield_10003 float NULL,
	fields_customfield_10004 float NULL,
	fields_customfield_10640 float NULL,
	fields_assignee_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_creator_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_reporter_emailAddress varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_originalEstimate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_remainingEstimate varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_originalEstimateSeconds float NULL,
	fields_timetracking_remainingEstimateSeconds float NULL,
	fields_aggregateprogress_percent float NULL,
	fields_progress_percent float NULL,
	fields_timetracking_timeSpent varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	fields_timetracking_timeSpentSeconds float NULL
    );"""

JiraFieldLabels_cols =  ['expand', '_id', 'self', 'key', 'fields_statuscategorychangedate'
, 'fields_issuetype_self', 'fields_issuetype_id'
, 'fields_issuetype_description', 'fields_issuetype_iconUrl'
, 'fields_issuetype_name', 'fields_issuetype_subtask'
, 'fields_issuetype_avatarId', 'fields_issuetype_hierarchyLevel'
, 'fields_parent_id', 'fields_parent_key', 'fields_parent_self'
, 'fields_parent_fields_summary', 'fields_parent_fields_status_self'
, 'fields_parent_fields_status_description'
, 'fields_parent_fields_status_iconUrl', 'fields_parent_fields_status_name'
, 'fields_parent_fields_status_id'
, 'fields_parent_fields_status_statusCategory_self'
, 'fields_parent_fields_status_statusCategory_id'
, 'fields_parent_fields_status_statusCategory_key'
, 'fields_parent_fields_status_statusCategory_colorName'
, 'fields_parent_fields_status_statusCategory_name'
, 'fields_parent_fields_priority_self'
, 'fields_parent_fields_priority_iconUrl'
, 'fields_parent_fields_priority_name', 'fields_parent_fields_priority_id'
, 'fields_parent_fields_issuetype_self', 'fields_parent_fields_issuetype_id'
, 'fields_parent_fields_issuetype_description'
, 'fields_parent_fields_issuetype_iconUrl'
, 'fields_parent_fields_issuetype_name'
, 'fields_parent_fields_issuetype_subtask'
, 'fields_parent_fields_issuetype_avatarId'
, 'fields_parent_fields_issuetype_hierarchyLevel', 'fields_timespent'
, 'fields_project_self', 'fields_project_id', 'fields_project_key'
, 'fields_project_name', 'fields_project_projectTypeKey'
, 'fields_project_simplified', 'fields_project_avatarUrls_48x48'
, 'fields_project_avatarUrls_24x24', 'fields_project_avatarUrls_16x16'
, 'fields_project_avatarUrls_32x32', 'fields_fixVersions'
, 'fields_aggregatetimespent', 'fields_customfield_10112'
, 'fields_resolution', 'fields_customfield_10113', 'fields_customfield_10630'
, 'fields_customfield_10114_hasEpicLinkFieldDependency'
, 'fields_customfield_10114_showField'
, 'fields_customfield_10114_nonEditableReason_reason'
, 'fields_customfield_10114_nonEditableReason_message'
, 'fields_customfield_10500_self', 'fields_customfield_10500_value'
, 'fields_customfield_10500_id', 'fields_customfield_10621'
, 'fields_customfield_10626', 'fields_customfield_10627'
, 'fields_resolutiondate', 'fields_customfield_10628'
, 'fields_customfield_10629', 'fields_workratio'
, 'fields_issuerestriction_shouldDisplay', 'fields_watches_self'
, 'fields_watches_watchCount', 'fields_watches_isWatching'
, 'fields_lastViewed', 'fields_created', 'fields_priority_self'
, 'fields_priority_iconUrl', 'fields_priority_name', 'fields_priority_id'
, 'fields_customfield_10100', 'fields_customfield_10101'
, 'fields_customfield_10300', 'fields_labels', 'fields_customfield_10620'
, 'fields_customfield_10610', 'fields_customfield_10611'
, 'fields_customfield_10613', 'fields_timeestimate'
, 'fields_aggregatetimeoriginalestimate', 'fields_versions'
, 'fields_customfield_10615', 'fields_customfield_10616'
, 'fields_customfield_10617', 'fields_customfield_10618'
, 'fields_customfield_10619', 'fields_issuelinks', 'fields_assignee_self'
, 'fields_assignee_accountId', 'fields_assignee_avatarUrls_48x48'
, 'fields_assignee_avatarUrls_24x24', 'fields_assignee_avatarUrls_16x16'
, 'fields_assignee_avatarUrls_32x32', 'fields_assignee_displayName'
, 'fields_assignee_active', 'fields_assignee_timeZone'
, 'fields_assignee_accountType', 'fields_updated', 'fields_status_self'
, 'fields_status_description', 'fields_status_iconUrl', 'fields_status_name'
, 'fields_status_id', 'fields_status_statusCategory_self'
, 'fields_status_statusCategory_id', 'fields_status_statusCategory_key'
, 'fields_status_statusCategory_colorName'
, 'fields_status_statusCategory_name', 'fields_components'
, 'fields_timeoriginalestimate', 'fields_description'
, 'fields_customfield_10005', 'fields_customfield_10600', 'fields_security'
, 'fields_customfield_10602', 'fields_customfield_10603'
, 'fields_aggregatetimeestimate', 'fields_attachment'
, 'fields_customfield_10604', 'fields_customfield_10605'
, 'fields_customfield_10606', 'fields_customfield_10607'
, 'fields_customfield_10608', 'fields_customfield_10609', 'fields_summary'
, 'fields_creator_self', 'fields_creator_accountId'
, 'fields_creator_avatarUrls_48x48', 'fields_creator_avatarUrls_24x24'
, 'fields_creator_avatarUrls_16x16', 'fields_creator_avatarUrls_32x32'
, 'fields_creator_displayName', 'fields_creator_active'
, 'fields_creator_timeZone', 'fields_creator_accountType', 'fields_subtasks'
, 'fields_customfield_10120', 'fields_reporter_self'
, 'fields_reporter_accountId', 'fields_reporter_avatarUrls_48x48'
, 'fields_reporter_avatarUrls_24x24', 'fields_reporter_avatarUrls_16x16'
, 'fields_reporter_avatarUrls_32x32', 'fields_reporter_displayName'
, 'fields_reporter_active', 'fields_reporter_timeZone'
, 'fields_reporter_accountType', 'fields_aggregateprogress_progress'
, 'fields_aggregateprogress_total', 'fields_customfield_10000'
, 'fields_customfield_10001', 'fields_customfield_10200'
, 'fields_customfield_10641', 'fields_customfield_10400'
, 'fields_customfield_10632', 'fields_customfield_10115'
, 'fields_customfield_10116', 'fields_environment'
, 'fields_customfield_10117', 'fields_customfield_10634', 'fields_duedate'
, 'fields_progress_progress', 'fields_progress_total'
, 'fields_comment_comments', 'fields_comment_self'
, 'fields_comment_maxResults', 'fields_comment_total'
, 'fields_comment_startAt', 'fields_votes_self', 'fields_votes_votes'
, 'fields_votes_hasVoted', 'fields_worklog_startAt'
, 'fields_worklog_maxResults', 'fields_worklog_total'
, 'fields_worklog_worklogs', 'fields_resolution_self', 'fields_resolution_id'
, 'fields_resolution_description', 'fields_resolution_name'
, 'fields_customfield_10500', 'fields_customfield_10629_self'
, 'fields_customfield_10629_value', 'fields_customfield_10629_id'
, 'fields_priority', 'fields_customfield_10619_self'
, 'fields_customfield_10619_value', 'fields_customfield_10619_id'
, 'fields_assignee', 'fields_customfield_10002', 'fields_customfield_10003'
, 'fields_customfield_10004', 'fields_customfield_10640'
, 'fields_assignee_emailAddress', 'fields_creator_emailAddress'
, 'fields_reporter_emailAddress', 'fields_timetracking_originalEstimate'
, 'fields_timetracking_remainingEstimate'
, 'fields_timetracking_originalEstimateSeconds'
, 'fields_timetracking_remainingEstimateSeconds'
, 'fields_aggregateprogress_percent', 'fields_progress_percent'
, 'fields_timetracking_timeSpent', 'fields_timetracking_timeSpentSeconds']
