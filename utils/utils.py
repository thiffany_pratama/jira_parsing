import json,sys
from datetime import datetime, timedelta

def check_mapr_path(log_engine, conn = None, table_name=''):
    with open('config.json') as f:
        list_path = json.load(f)['maprdb_path']
    db_path = list_path[table_name]

    if conn.is_store_exists(db_path):
        log_engine.log_info(f"TABLE CHECKING | {table_name} | Table exists - {sys.exc_info()[0]} - {sys.exc_info()[1]}")

    else:
        conn.create_store(db_path)
    conn.close()


def generate_timing(start, end):
    start = datetime.strptime(start, "%Y-%m-%d %H:%M")
    end = datetime.strptime(end, "%Y-%m-%d %H:%M")
    delta = timedelta(minutes=30)

    times = []
    while start <= end:
        times.append(start.strftime('%Y-%m-%d %H:%M'))
        start += delta
    time_range =[]
    for x in range(len(times)-1):
        after = x+1
        time_range.append(f"""{times[x]}||{times[after]}""")
    return time_range
    