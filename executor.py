from utils.builder import *
from utils.utils import *
from utils.jql import *
from utils.data_processing import *
import pandas as pd
import sqlalchemy
pd.options.display.max_columns = None
pd.options.display.max_rows = None


def jira_query(log_engine, starting, ending):
    
    time_range = generate_timing(starting, ending)
    
    jira_conn = JiraBuilder()
    
    mssqlserver = MsSqlServerBuilder().get_conn_prop()
    
    list_tbls = ListTablesBuilder().get_tables_list()
    log_engine.log_info(f"CONNECTION ESTABLISHED")
    
    for tr in time_range:
        start_ = tr.split('||')[0]
        end_ = tr.split('||')[1]
        log_engine.log_info(f"PROCESSING {tr}")
        
        data_processor = DataProcessor(log_engine=log_engine,
                               time_range = tr,
                               jira_conn = jira_conn,
                               mssqlserver = mssqlserver)
        
        
        data_processor.df1_processing()
        if len(data_processor.df1) == 0:
            continue
        data_processor.df1_write()

        data_processor.df2_processing()
        data_processor.df2_write()

        data_processor.df3_4_processing()
        data_processor.df3_write()

        data_processor.df5_processing()
        data_processor.df5_write()

        data_processor.df6_processing()
        data_processor.df6_write()
        
        data_processor.close_conn()
        
        
        
        